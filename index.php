<?php

error_reporting(E_ALL);
ini_set("display_startup_errors", "1");
ini_set("display_errors", "1");

//Includes
require('abstractTreeView.class.php');
require('myTreeView.class.php');
require('DBadapter/PDOadapter.php');
require('Model/TreeView.php');

// Autoload vendor classes
require "vendor/autoload.php";

// Load .env values
(Dotenv\Dotenv::createImmutable(__DIR__))->load();


$treeView = new myTreeView();

/** Simple routing logic **/
if (!isset($_GET['ajax-version'])) {
    $treeView->showCompleteTree();
} else {
    if (isset($_GET['node_id'])) {
        $treeView->fetchAjaxTreeNode($_GET['node_id']);
    } else {
        $treeView->showAjaxTree();
    }
}


