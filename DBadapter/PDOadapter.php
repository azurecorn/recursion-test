<?php

declare(strict_types=1);

/**
 * Class PDOadapter
 * @package App\DBadapter
 */
class PDOadapter
{
    private $conn;

    public function __construct()
    {
        try{
            $this->conn = new PDO(
                'mysql:host='.getenv('DB_SERVER').';dbname='.getenv('DB_DATABASE'),
                getenv('DB_USERNAME'), getenv('DB_PASSWORD'));
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Returns connection object
     *
     * @return PDO
     */
    public function getConnection()
    {
        return $this->conn;
    }


}
