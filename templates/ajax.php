<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="../css/style.css">

    <title>TreeView App - Ajax Version</title>
</head>
<body>
    <div class="container-fluid">
        <div class="row mt-4">
            <div class="col offset-lg-2 col-lg-8">
                <h1>TreeView App</h1>
            </div>
            <div class="col offset-lg-2 col-lg-8">
                <ul id="treeview">
                    <?php echo $nodesHTML ?>
                </ul>
            </div>
        </div>
        <div class="row mt-4 mb-4">
            <div class="col offset-lg-2 col-lg-8">
                <h5><a class="btn btn-success" href="/">Regular Version</a></h5>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script>
        $(document).ready(function () {
            $("body").on('click', 'li', function (e) {
                e.preventDefault();

                let node = $(this);

                //handle case when node hasn't children
                if (!node.hasClass('parent')) {
                    return false;
                }

                //handle case when data is already loaded via AJAX
                if (node.next('ul').length) {
                    node.next("ul").toggle("slow");
                    if (node.hasClass("closse"))
                        node.removeClass("closse");
                    else
                        node.addClass("closse");
                    return false;
                }

                $.ajax({
                    url: '/',
                    method: 'get',
                    data: {node_id: node.attr('id'), 'ajax-version': 1},
                    success: function (data, textStatus, jqXHR) {
                        node.after(data);
                        node.addClass("closse");
                    },
                    error: function (xhr, status, errorThrown) {
                        let response = jQuery.parseJSON(xhr.responseText);
                        console.log(response);
                    }
                })
            });
        });
    </script>
</body>
</html>
