<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="../css/style.css">

    <title>TreeView App</title>
</head>
<body>
    <div class="container-fluid">
        <div class="row mt-4">
            <div class="col offset-lg-2 col-lg-8">
                <h1>TreeView App</h1>
            </div>
            <div class="col offset-lg-2 col-lg-8">
                <ul id="treeview">
                    <?php echo $nodesHTML ?>
                </ul>
            </div>
        </div>
        <div class="row mt-4 mb-4">
            <div class="col offset-lg-2 col-lg-8">
                <h5><a class="btn btn-success" href="/?ajax-version">AJAX version</a></h5>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script>
        $(document).ready(function () {

            $("#treeview .parent").click(function (e) {
                e.stopPropagation();
                $(this).find(">ul").toggle("slow");
                if ($(this).hasClass("closse"))
                    $(this).removeClass("closse");
                else
                    $(this).addClass("closse");
            });
        });
    </script>
</body>
</html>
