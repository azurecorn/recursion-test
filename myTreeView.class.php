<?php

declare(strict_types=1);

/**
 * Implement your code here
 * Feel free to remove the echos :)
 */
class myTreeView extends abstractTreeView
{

    private $pdo;
    private $treeView;

    public function __construct()
    {
        $this->pdo = new PDOadapter();
        $this->treeView = new TreeView($this->pdo);
    }

    /**
     * Regular version: A
     */
    public function showCompleteTree(): void
    {
        $treeViewData = $this->treeView->get();

        $treeViewtructure = $this->buildTree($treeViewData);

        $nodesHTML = $this->buildHTMLtree($treeViewtructure);

        include 'templates/index.php';
    }

    /**
     * Ajax version: B
     */
    public function showAjaxTree(): void
    {
        $rootNodesParentId = 0;

        $treeViewData = $this->treeView->get($rootNodesParentId);

        $treeViewtructure = $this->buildTree($treeViewData);

        $nodesHTML = $this->buildAjaxHTMLtree($treeViewtructure);

        include 'templates/ajax.php';

    }

    /**
     * Provides node data for AJAX version
     *
     * @param $entryId
     * @return string
     */
    public function fetchAjaxTreeNode($entryId): string
    {
        $treeViewData = $this->treeView->get($entryId);

        $treeViewtructure = $this->formatLanguageData($treeViewData);

        $nodesHTML = $this->buildAjaxHTMLtree($treeViewtructure, true);

        echo $nodesHTML;

        die;
    }

    /**
     * Recursive building HTML structure
     *
     * @param array $treeViewData
     * @return string
     */
    private function buildHTMLtree(array $treeViewData): string
    {
        $html = '';

        foreach ($treeViewData as $row) {
            $nodeHasChildren = isset($row['children']);

            $html .= '<li ' . ($nodeHasChildren ? 'class="parent"' : '') . '>';
            $html .= (isset($row['ger']) ? $row['ger'] : $row['eng']);

            if ($nodeHasChildren) {
                $html .= '<ul>';
                $children = $this->buildHTMLtree($row['children']);
                $html .= $children . '</ul>';
            }

            $html .= '</li>';
        }

        return $html;
    }

    /**
     * Recursive building HTML structure for AJAX version
     *
     * @param array $treeViewData
     * @return string
     */
    private function buildAjaxHTMLtree(array $treeViewData, $append = false): string
    {
        $html = '';

        if ($append && count($treeViewData)) {
            $html .= '<ul>';
        }

        foreach ($treeViewData as $row) {

            $html .= '<li id="' . $row['entry_id'] . '" ' . ($row['children_count'] ? ' class="parent" ' : '') . '>';
            $html .= (isset($row['ger']) ? $row['ger'] : $row['eng']);

            $html .= '</li>';
        }

        if ($append && count($treeViewData)) {
            $html .= '</ul>';
        }

        return $html;
    }

    /**
     * Recursive formatting and structuring data from DB for further processing
     *
     * @param array $treeViewData
     * @param int $parentId
     * @return array
     */
    private function buildTree(array $treeViewData, int $parentId = 0): array
    {
        $tree = [];

        $formattedData = $this->formatLanguageData($treeViewData);

        foreach ($formattedData as $row) {
            if ($row['parent_entry_id'] == $parentId) {

                $children = $this->buildTree($treeViewData, (int)$row['entry_id']);

                if ($children) {
                    $row['children'] = $children;
                }
                $tree[] = $row;
            }
        }

        return $tree;
    }

    /**
     * Merges `eng` and `ger` languages to one entry
     *
     * @param array $treeViewData
     * @return array
     */
    private function formatLanguageData(array $treeViewData): array
    {
        $formattedData = [];

        foreach ($treeViewData as $data) {
            $fd = &$formattedData[$data['entry_id']];

            $fd['entry_id'] = $data['entry_id'];
            $fd['parent_entry_id'] = $data['parent_entry_id'];
            $fd['children_count'] = $data['children_count'];
            $fd[$data['lang']] = $data['name'];
        }

        return $formattedData;
    }
}
