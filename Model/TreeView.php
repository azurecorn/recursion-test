<?php

declare(strict_types=1);

require_once('DBadapter/PDOadapter.php');

/**
 * Class TreeView is our app main entity.
 */
class TreeView
{
    private $conn;

    /**
     * Address constructor.
     * @param PDOadapter $dbAdapter
     */
    public function __construct(PDOadapter $dbAdapter)
    {
        $this->conn = $dbAdapter->getConnection();
    }

    /**
     * Returns entries
     *
     * @param int|null $parentNodeId
     * @return array|null
     */
    public function get($parentNodeId = null): ?array
    {

        if ($parentNodeId === null) {
            $conditionalNodeId = '';
        } else {
            $conditionalNodeId = 'WHERE te.parent_entry_id = ' . (int)$parentNodeId;
        }

        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "SELECT
                       te.entry_id, 
                       te.parent_entry_id, 
                       tel.lang, 
                       tel.name,
                       count(te2.entry_id) as children_count
                    FROM tree_entry te 
                    LEFT JOIN tree_entry_lang tel ON te.entry_id = tel.entry_id
                    LEFT JOIN tree_entry te2 ON te.entry_id = te2.parent_entry_id
                    {$conditionalNodeId}
                    GROUP BY te.entry_id, te.entry_id, te.parent_entry_id, tel.lang, tel.name
                    ORDER BY tel.name";

            $stmt = $this->conn->prepare($sql);

            $stmt->execute();
            $entries = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $this->conn = null;

            return $entries;

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}
